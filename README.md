# yaml-pretty-extras

This library adds a ```toPrettyYaml``` typeclass and helper functions.

## Example Usage

```{.haskell}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Yaml.Pretty.Extras
import GHC.Generics

data Foo = Foo {
  bar :: Text,
  quux :: Text
} deriving (Eq, Show, FromJSON, ToJSON)

instance ToPrettyYaml Foo where
  fieldOrder = const ["quux", "bar"]

main = encodeFilePretty "foo.yaml" (Foo "bar" "quux") -- prints out "quux: quux\nbar: bar"
```
