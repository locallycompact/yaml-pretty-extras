# Changelog for yaml-pretty-extras

## v0.0.2.2

Drop MonadThrow constraints from file IO functions
Drop decodeFileThrow, it's in upstream yaml now

## v0.0.2.1

Add lens helpers, transformFile, overFile, traverseOfFile, sanitizeFile

## v0.0.2.0

Add haddock documentation.
Drop PrettyYamlException type.

## v0.0.1.3

Add decodeFileThrow, decodeFileThrowLogged and encodeFilePrettyLogged functions
for use with RIO

## v0.0.1.2

Base on RIO
Add displayPrettyYaml function

## v0.0.1.1

Add dropNull to ToPrettyYaml typeclass

## v0.0.1.0

Added ToPrettyYaml typeclass
Added encodePrettyFile function
