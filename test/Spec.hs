{-# LANGUAGE TemplateHaskell #-}

import           Data.Yaml.Pretty.Extras
import           GHC.Generics
import           Lens.Micro.Platform hiding ((.=))
import           RIO
import qualified RIO.ByteString                as BS
import           Test.Hspec


data Person = Person {
  _mood :: Text,
  _job  :: Text
} deriving (Eq, Generic, Show)

instance FromJSON Person where
  parseJSON (Object v) = Person
    <$> v .: "mood"
    <*> v .: "job"

instance ToJSON Person where
  toJSON x = object ["mood" .= _mood x, "job" .= _job x]

$(makeLenses ''Person)

data Conf = Conf {
  _name :: Text,
  _kind :: Text,
  _people :: [Person]
} deriving (Eq, Generic, Show)

instance FromJSON Conf where
  parseJSON (Object v) = Conf
    <$> v .: "name"
    <*> v .: "kind"
    <*> v .: "people"

instance ToJSON Conf where
  toJSON x = object ["name" .= _name x, "kind" .= _kind x, "people" .= _people x]

$(makeLenses ''Conf)

instance ToPrettyYaml Conf where
  fieldOrder = const ["name", "kind", "people", "mood", "job"]

main :: IO ()
main = hspec $ describe "Data.Yaml.Pretty.Extras" $ it "prints correctly" $ do
  Just x <- decodeFileThrow "test/mock.yaml" :: IO (Maybe Conf)
  BS.readFile "test/mock.yaml" `shouldReturn` toPrettyYaml x
